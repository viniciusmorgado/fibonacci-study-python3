#!/usr/share/bin/python3


"""
Função cria um loop fibonacci de acordo com o input na variavel limit,
iniciando em 0 e 1.
"""


def fibonacci(limit):
    result = [0, 1]
    while result[-1] < limit:
        result.append(result[-2] + result[-1])
    return  result

# Corpo principal do script Python invocando a função fibonacci com intervalo de 200000.
if __name__ == '__main__':
    for fib in fibonacci(200000):
        print(fib)
