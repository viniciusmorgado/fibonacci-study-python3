#!/usr/share/python3


def fibonacci(value_amount, sequence=(0, 1)):
    # stop condition
    if len(sequence) == value_amount:
        return sequence
    return fibonacci(value_amount, sequence + (sum(sequence[-2:]),))


if __name__ == '__main__':
    # List first 20 numbers of sequence.
    for fib in fibonacci(20):
        print(fib)
