#!/usr/share/bin/python3

# Entre com a quantidade de elementos que você deseja colocar na sequência Fibonacci:
"""
Função cria um loop fibonacci de acordo com o input na variavel limit,
iniciando em 0 e 1.
"""

# def help():
#    print(('Entrada inválida!'))

def fibonacci(amount):
    result = [0, 1]
    for _ in range(2, amount):
        result.append(sum(result[-2:]))
    return  result

# Corpo principal do script Python invocando a função fibonacci com intervalo segundo o input do usuário.
if __name__ == '__main__':
    mount = int(input('Entre com a quantidade de elementos que você deseja colocar na sequência Fibonacci: '))
    for fib in fibonacci(mount):
        print(fib)
