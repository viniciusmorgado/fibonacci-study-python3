#!/usr/share/bin/python3


# Function to print error message in invalid entry's.
def error_help():
    print('Error: invalid entry, the value need to be int!')


# Function to cal fibonacci sequence with a for range().
def fibonacci(amount):
    result = [0, 1]
    for _ in range(2, amount):
        result.append(sum(result[-2:]))
    return result


# Main function responsible to execute fibonacci() function with the user input.
if __name__ == '__main__':
    mount = input('Please, enter with value for Fibonacci sequence:')
    if "." in mount:
        error_help()
    else:
        for fib in fibonacci(int(mount)):
            print(fib)
