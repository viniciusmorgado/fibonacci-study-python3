#!/usr/share/bin/python3


"""
Função cria um loop fibonacci de acordo com o input na variavel limit,
iniciando em 0 e 1.
"""


def fibonacci(limit):
    value_last = 0
    value_end = 1
    print(f'{value_last},{value_end}', end=',')
    while value_end < limit:
        nex = value_last + value_end
        print(nex, end=',')
        value_last = value_end
        value_end = nex


# Função principal do script Python invocando a função fibonacci.
if __name__ == '__main__':
    fibonacci(20000)
